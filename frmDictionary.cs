﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class frmDictionary : Form
    {
        public string strWord;

        public frmDictionary()
        {
            InitializeComponent();
            TextSearchAutoComplete();

            txtSearch.Click += new System.EventHandler(txtSearch_Click);
            txtSearch.Enter += new System.EventHandler(txtSearch_Click);
            txtSearch.KeyDown += new KeyEventHandler(txtSearch_KeyDown);
        }

        private void txtSearch_Click(object sender, System.EventArgs e) {
            txtSearch.SelectAll();
            TextSearchAutoComplete();
        }

        private void TextSearchAutoComplete()
        {
            using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
            {
                using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("select word from dictionary", sqlite_conn))
                {
                    txtSearch.AutoCompleteCustomSource.Clear();
                    txtSearch.AutoCompleteMode = AutoCompleteMode.Suggest;
                    txtSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection col = new AutoCompleteStringCollection();

                    try
                    {
                        sqlite_conn.Open();
                        using (System.Data.SQLite.SQLiteDataReader sqlite_datareader = sqlite_cmd.ExecuteReader())
                        {

                            while (sqlite_datareader.Read())
                            {
                                string strAppend = sqlite_datareader.GetString(0);
                                col.Add(strAppend);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    txtSearch.AutoCompleteCustomSource = col;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                search();   
            }
        }

        private void search()
        {
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;

            using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
            {
                try
                {
                    // open the connection:
                    sqlite_conn.Open();

                    using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("SELECT word, figureOfSpeech, definition FROM dictionary where word = '" + txtSearch.Text + "'", sqlite_conn))
                    {                        
                        // Execute the SQLiteCommand object using a DataReader-Object
                        using (System.Data.SQLite.SQLiteDataReader sqlite_datareader = sqlite_cmd.ExecuteReader())
                        {
                            // The SQLiteDataReader allows us to run through the result lines:
                            if (sqlite_datareader.Read()) // Read() returns true if there is still a result line to read
                            {
                                strWord = sqlite_datareader.GetString(0); // numbers in getString refer to numbers of columns
                                string strFigure = sqlite_datareader.GetString(1);
                                string strDefinition = sqlite_datareader.GetString(2);

                                rchTxtResult.Text = ("English: " + strWord + " " + strFigure + "\r\n" + "Definition: " + strDefinition);
                                rchTxtResult.Find(strFigure, RichTextBoxFinds.MatchCase);
                                rchTxtResult.SelectionFont = new Font(rchTxtResult.Font, FontStyle.Italic);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rchTxtResult.Text = ex.ToString();
                }
            }
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuAdd_Click(object sender, EventArgs e)
        {
            frmAddWord frmAdd = new frmAddWord();
            frmAdd.ShowDialog();
            TextSearchAutoComplete();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete the word '"+strWord+"' from the dictionary?","Delete Word", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
                {
                    try
                    {
                        sqlite_conn.Open();                       
                        using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("Delete FROM dictionary where word = '" + strWord + "'", sqlite_conn))
                        {
                            sqlite_cmd.ExecuteNonQuery();

                            rchTxtResult.Text = "Word Deleted";
                            txtSearch.Text = "";

							btnEdit.Enabled = false;
							btnDelete.Enabled = false;
                        }
                        sqlite_conn.Close();
                        
                        TextSearchAutoComplete();
                    }
                    catch (Exception ex)
                    {
                        rchTxtResult.Text = ex.ToString();
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (frmEdit frmEdit1 = new frmEdit(strWord))
            {
                frmEdit1.ShowDialog();
            }
            TextSearchAutoComplete();
            search();
        }

        private void aboutDictionaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Enter text in the search field. As you enter text in the search field suggestions\n" +
            "of words are displayed. To display the definition, select the word from the list.\n\n" +
            "To add a word to the dictionary, navigate to File -> Add Word.\n\n" +
            "To edit a word, click on the Edit button and to delete a word, click on the delete button.", "About Dictionary");
        }
    }
}