﻿namespace Dictionary
{
    partial class frmEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEdit));
            this.txtDef = new System.Windows.Forms.TextBox();
            this.lblDef = new System.Windows.Forms.Label();
            this.lblFigure = new System.Windows.Forms.Label();
            this.lblWord = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.txtFigure = new System.Windows.Forms.TextBox();
            this.txtWord = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtDef
            // 
            this.txtDef.Location = new System.Drawing.Point(103, 81);
            this.txtDef.Multiline = true;
            this.txtDef.Name = "txtDef";
            this.txtDef.Size = new System.Drawing.Size(169, 130);
            this.txtDef.TabIndex = 13;
            // 
            // lblDef
            // 
            this.lblDef.AutoSize = true;
            this.lblDef.Location = new System.Drawing.Point(13, 84);
            this.lblDef.Name = "lblDef";
            this.lblDef.Size = new System.Drawing.Size(54, 13);
            this.lblDef.TabIndex = 12;
            this.lblDef.Text = "Definition:";
            // 
            // lblFigure
            // 
            this.lblFigure.AutoSize = true;
            this.lblFigure.Location = new System.Drawing.Point(12, 53);
            this.lblFigure.Name = "lblFigure";
            this.lblFigure.Size = new System.Drawing.Size(91, 13);
            this.lblFigure.TabIndex = 11;
            this.lblFigure.Text = "Figure of Speech:";
            // 
            // lblWord
            // 
            this.lblWord.AutoSize = true;
            this.lblWord.Location = new System.Drawing.Point(12, 26);
            this.lblWord.Name = "lblWord";
            this.lblWord.Size = new System.Drawing.Size(36, 13);
            this.lblWord.TabIndex = 10;
            this.lblWord.Text = "Word:";
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(16, 217);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(256, 23);
            this.btnEdit.TabIndex = 9;
            this.btnEdit.Text = "Edit Word";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtFigure
            // 
            this.txtFigure.Location = new System.Drawing.Point(103, 50);
            this.txtFigure.Name = "txtFigure";
            this.txtFigure.Size = new System.Drawing.Size(169, 20);
            this.txtFigure.TabIndex = 8;
            // 
            // txtWord
            // 
            this.txtWord.Location = new System.Drawing.Point(103, 23);
            this.txtWord.Name = "txtWord";
            this.txtWord.ReadOnly = true;
            this.txtWord.Size = new System.Drawing.Size(169, 20);
            this.txtWord.TabIndex = 7;
            // 
            // frmEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txtDef);
            this.Controls.Add(this.lblDef);
            this.Controls.Add(this.lblFigure);
            this.Controls.Add(this.lblWord);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.txtFigure);
            this.Controls.Add(this.txtWord);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEdit";
            this.Text = "Edit Word";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDef;
        private System.Windows.Forms.Label lblDef;
        private System.Windows.Forms.Label lblFigure;
        private System.Windows.Forms.Label lblWord;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.TextBox txtFigure;
        private System.Windows.Forms.TextBox txtWord;
    }
}