﻿namespace Dictionary
{
    partial class frmAddWord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddWord));
            this.txtWord = new System.Windows.Forms.TextBox();
            this.txtFigure = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblWord = new System.Windows.Forms.Label();
            this.lblFigure = new System.Windows.Forms.Label();
            this.lblDef = new System.Windows.Forms.Label();
            this.txtDef = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtWord
            // 
            this.txtWord.Location = new System.Drawing.Point(103, 33);
            this.txtWord.Name = "txtWord";
            this.txtWord.Size = new System.Drawing.Size(169, 20);
            this.txtWord.TabIndex = 0;
            // 
            // txtFigure
            // 
            this.txtFigure.Location = new System.Drawing.Point(103, 60);
            this.txtFigure.Name = "txtFigure";
            this.txtFigure.Size = new System.Drawing.Size(169, 20);
            this.txtFigure.TabIndex = 1;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(16, 227);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(256, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "Add Word";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblWord
            // 
            this.lblWord.AutoSize = true;
            this.lblWord.Location = new System.Drawing.Point(12, 36);
            this.lblWord.Name = "lblWord";
            this.lblWord.Size = new System.Drawing.Size(36, 13);
            this.lblWord.TabIndex = 3;
            this.lblWord.Text = "Word:";
            // 
            // lblFigure
            // 
            this.lblFigure.AutoSize = true;
            this.lblFigure.Location = new System.Drawing.Point(12, 63);
            this.lblFigure.Name = "lblFigure";
            this.lblFigure.Size = new System.Drawing.Size(91, 13);
            this.lblFigure.TabIndex = 4;
            this.lblFigure.Text = "Figure of Speech:";
            // 
            // lblDef
            // 
            this.lblDef.AutoSize = true;
            this.lblDef.Location = new System.Drawing.Point(13, 94);
            this.lblDef.Name = "lblDef";
            this.lblDef.Size = new System.Drawing.Size(54, 13);
            this.lblDef.TabIndex = 5;
            this.lblDef.Text = "Definition:";
            // 
            // txtDef
            // 
            this.txtDef.Location = new System.Drawing.Point(103, 91);
            this.txtDef.Multiline = true;
            this.txtDef.Name = "txtDef";
            this.txtDef.Size = new System.Drawing.Size(169, 130);
            this.txtDef.TabIndex = 6;
            // 
            // frmAddWord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txtDef);
            this.Controls.Add(this.lblDef);
            this.Controls.Add(this.lblFigure);
            this.Controls.Add(this.lblWord);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtFigure);
            this.Controls.Add(this.txtWord);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAddWord";
            this.Text = "Add Word";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtWord;
        private System.Windows.Forms.TextBox txtFigure;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblWord;
        private System.Windows.Forms.Label lblFigure;
        private System.Windows.Forms.Label lblDef;
        private System.Windows.Forms.TextBox txtDef;
    }
}