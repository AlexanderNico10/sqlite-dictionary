SQLite Dictionary
===

Licensing Information: READ LICENSE
---

Project source: https://AlexanderNico10@bitbucket.org/AlexanderNico10/sqlite-dictionary.git
---

File List
---

```
Dictionary.csproj
frmAddWord.cs
frmAddWord.Designer.cs
frmAddWord.resx
frmDictionary.cs
frmDictionary.Designer.cs
frmDictionary.resx
frmEdit.cs
frmEdit.Designer.cs
frmEdit.resx
Program.cs
LICENSE
README.md
.\bin
.\obj
```

```
\bin\x64\Debug

databaseDictionary.db
Dictionary.exe
Dictionary.pdb
Dictionary.vshost.exe
Dictionary.vshost.exe.manifest
EntityFramework.dll
Installer.exe
Installer.pdb
northwindEF.db
SQLite.Designer.dll
SQLite.Designer.pdb
System.Data.SQLite.dll
System.Data.SQLite.dll.config
System.Data.SQLite.EF6.dll
System.Data.SQLite.EF6.pdb
System.Data.SQLite.Linq.dll
System.Data.SQLite.Linq.pdb
System.Data.SQLite.pdb
```

How to run application
---

This application makes use of a SQLite database system core for x64
platforms. Import the application into Visual Studio, verify that
Debugging solution platform is set to x64, and the platform target
is set to x64 in the properties file, then start debugging.