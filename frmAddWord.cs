﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class frmAddWord : Form
    {
        public frmAddWord()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
            {
                try
                {
                    // open the connection:
                    sqlite_conn.Open();
                    using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("select word from dictionary where word ='" + txtWord.Text + "'", sqlite_conn))
                    {
                        using (System.Data.SQLite.SQLiteDataReader sqlite_datareader = sqlite_cmd.ExecuteReader())
                        {
                            if (sqlite_datareader.Read()) // Read() returns true if there is still a result line to read
                            {
                                MessageBox.Show("This word is already in the dictionary.", "Word already exists");
                                txtDef.Text = "";
                                txtFigure.Text = "";
                                txtWord.Text = "";
                            }
                            else if (txtDef.Text == String.Empty ||
                                     txtFigure.Text == String.Empty ||
                                     txtWord.Text == String.Empty)
                            {
                                MessageBox.Show("All fields are required.", "Enter all fields");
                                txtDef.Text = "";
                                txtFigure.Text = "";
                                txtWord.Text = "";
                            }
                            else
                            {
                                sqlite_datareader.Close();
                                // insert something into table
                                sqlite_cmd.CommandText = "INSERT INTO dictionary (word, figureOfSpeech, definition) "
                                + "VALUES (@word, @figureOfSpeech, @definition);";

                                sqlite_cmd.Parameters.AddWithValue("@word", txtWord.Text);
                                sqlite_cmd.Parameters.AddWithValue("@figureOfSpeech", txtFigure.Text);
                                sqlite_cmd.Parameters.AddWithValue("@definition", txtDef.Text);
                                // execute the SQL
                                sqlite_cmd.ExecuteNonQuery();

                                MessageBox.Show("Word added.", "Word Added");
                                this.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error");
                }
            }
        }
    }
}
