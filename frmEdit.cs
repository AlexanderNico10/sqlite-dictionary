﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class frmEdit : Form
    {
        public frmEdit(string word)
        {
            InitializeComponent();
            this.ActiveControl = txtDef;

            using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
            {
                try
                {
                    // open the connection:
                    sqlite_conn.Open();
                    
                    using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("SELECT word, figureOfSpeech, definition FROM dictionary where word = '" + word + "'", sqlite_conn))
                    {
                        // Execute the SQLiteCommand object using a DataReader-Object
                        using (System.Data.SQLite.SQLiteDataReader sqlite_datareader = sqlite_cmd.ExecuteReader())
                        {
                                                        // The SQLiteDataReader allows us to run through the result lines:
                            if (sqlite_datareader.Read()) // Read() returns true if there is still a result line to read
                            {
                                string strWord = sqlite_datareader.GetString(0); // numbers in getString refer to numbers of columns
                                string strFigure = sqlite_datareader.GetString(1);
                                string strDefinition = sqlite_datareader.GetString(2);

                                txtWord.Text = strWord;
                                txtFigure.Text = strFigure;
                                txtDef.Text = strDefinition;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Error");
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (System.Data.SQLite.SQLiteConnection sqlite_conn = new System.Data.SQLite.SQLiteConnection("Data Source=databaseDictionary.db;Version=3;New=True;Compress=True;"))
            {
                try
                {
                    // open the connection:
                    sqlite_conn.Open();
                    using (System.Data.SQLite.SQLiteCommand sqlite_cmd = new System.Data.SQLite.SQLiteCommand("select word from dictionary where word ='" + txtWord.Text + "'", sqlite_conn))
                    {
                        using (System.Data.SQLite.SQLiteDataReader sqlite_datareader = sqlite_cmd.ExecuteReader())
                        {
                            if (txtDef.Text == String.Empty ||
                                     txtFigure.Text == String.Empty ||
                                     txtWord.Text == String.Empty)
                            {
                                MessageBox.Show("All fields are required.", "Enter all fields");
                                txtFigure.Text = "";
                                txtWord.Text = "";
                            }
                            else
                            {
                                sqlite_datareader.Close();
                                // insert something into table
                                sqlite_cmd.CommandText = "update dictionary set figureOfSpeech=@figureOfSpeech, definition=@definition "
                                + "where word=@word";

                                sqlite_cmd.Parameters.AddWithValue("@word", txtWord.Text);
                                sqlite_cmd.Parameters.AddWithValue("@figureOfSpeech", txtFigure.Text);
                                sqlite_cmd.Parameters.AddWithValue("@definition", txtDef.Text);
                                // execute the SQL
                                sqlite_cmd.ExecuteNonQuery();

                                MessageBox.Show("Word updated.", "Word Updated");
                                this.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
    }
}
